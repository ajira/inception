module.exports = {
  title: 'Inception',
  base: '/inception/',
  dest: 'public',
  sourceDir: '../',
  themeConfig: {
    sidebar: [
      ['/', 'Introduction'],
      ['/vision', 'Vision'],
      ['/accelarators', 'Accelarators'],
      ['/anchors', 'Anchors'],
      ['/persona', 'User Personas'],
      ['/user-journey', 'User Journeys'],
      ['/existing-system', 'Existing System'],
      ['/features', 'Features'],
      ['/roadmap', 'Roadmap'],
      ['/tech-choices', 'Tech Choices']
    ]
  }
}