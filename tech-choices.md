# Tech Choices

## Backend
  * Programming Language
  * Framework(s) 
  * Libraries
### Rationale
  ....

## Frontend
  * Programming Language
  * Framework(s) 
  * Libraries
### Rationale
  ....

## Database(s)
  * ....
### Rationale
  ....

## CI
  * <<TOOL>>

## Deployment 
  * Cloud/On-Premise
  * Release Cycle

## Git Branching
  * Trunk or Feature Branch

## Other Tools
  ....