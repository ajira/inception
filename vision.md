# Product Vision

## Define the Problem - Elevator Pitch

::: tip 
Know the problem clearly. The best sign that we know the problem that we’re solving is if we can clearly state the problem in two sentences or less. We want to be certain that the problem exists before building anything. 
:::

## Who has the problem

::: tip
We also want to know someone that experiences the problem. Because, the people who experience the problem can give us a clue to how to solve it.
:::

## Why do they need this

## The intensity & frequency of the problem 

::: tip
This basically checks for 2 factors: how intense the problem is for your customer and how frequent they have this problem. In general, low intensity + low frequency is bad. And high intensity + high frequency is good.

Let’s use Uber as an example. The problem of being at point A and wanting to get to point B is a high intensity problem. So intense, that you probably bought a $20,000 vehicle to help you solve that problem. And for frequency: how often do you move more than a mile or walking distance? Happens almost everyday.
:::


## The Product is not

::: tip
Sometimes, it’s easier to describe something by telling what this thing is not or does not.
:::

## The Product does not